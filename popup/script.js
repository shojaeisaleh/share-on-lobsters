const btnElm = document.querySelector('#btn')
const titleElm = document.querySelector('#title')
const linkElm = document.querySelector('#link')

window.onload = () => {
	var state = {
		title: null,
		url: null,
	}

	const setInfo = () => {
		const { title, url } = state
		titleElm.innerText = title
		linkElm.innerText = url
	}

	const openLink = () => {
		let { title, url } = state
		title = encodeURIComponent(title)
		url = encodeURIComponent(url)
		window.open(`https://lobste.rs/stories/new?url=${url}&title=${title}`)
	}

	;(() => {
		browser.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
			for (const tab of tabs) {
				let { title, url } = tab
				state = {
					title,
					url,
				}
				setInfo()
			}
		})
	})()

	btnElm.addEventListener('click', openLink)
}
